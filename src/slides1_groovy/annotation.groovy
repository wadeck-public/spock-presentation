import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(excludes = "age")
class Person {
	String firstName
	String lastName
	Integer age
}