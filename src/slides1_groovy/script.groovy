Binding binding = new Binding()
binding.setVariable("foo", 2)
GroovyShell shell = new GroovyShell(binding)

Object value = shell.evaluate("""
	foo + 2
""")

println value