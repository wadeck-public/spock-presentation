package slides2_spock

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class AdditionTest3e extends Specification {
	def "add numbers: #a + #b = #expected"() {
		expect:
			Adder.add(a, b) == expected

		where:
			[a, b, expected] << sql.rows("select a, b, c from maxdata")
	}
}