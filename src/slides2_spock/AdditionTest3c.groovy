package slides2_spock

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class AdditionTest3c extends Specification {
	def "add numbers: #a + #b = #expected"() {
		expect:
			Adder.add(a, b) == expected

		where:
			[a, b, expected] << [
				[1, 1, 2],
				[1, -1, 0],
				[1, 4, 5],
				[6, 1, 7]
			]
	}
}