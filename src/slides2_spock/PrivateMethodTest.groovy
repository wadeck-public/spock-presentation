package slides2_spock

import spock.lang.Specification

class IncrTest {
	private int privateField = 42

	public int incrConstant(int a) {
		return a + privateField
	}
}

class PrivateMethodTest extends Specification {
	IncrTest incr = new IncrTest()

	def "incrConstant"() {
		given:
			incr.privateField = 43

		when:
			def actual = incr.incrConstant(12)

		then:
			actual == 12 + 43
	}
}