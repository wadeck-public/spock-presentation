package slides2_spock

import spock.lang.Specification

class AdditionTest2 extends Specification {
	def "add two numbers"() {
		expect:
			Adder.add(1, 2) == 3
	}
}
