package slides2_spock

import spock.lang.Specification

class AdditionTest3 extends Specification {
	def "add two numbers"() {
		expect:
			Adder.add(1, 1) == 2
			Adder.add(1, -1) == 0
			Adder.add(1, 4) == 5
			Adder.add(6, 1) == 7
	}
}