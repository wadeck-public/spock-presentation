package slides2_spock

import spock.lang.Specification

@Service
class TimeService {
	public long getTime(){
		return System.currentTimeMillis()
	}
}

@Service
class RandomService {

	@Autowired
	private TimeService timeService

	public long getNextRandom(){
		return timeService.getTime() + 42
	}
}

@SpringBootTest
class MockSpringTest extends Specification {
	@AutowiredMock // replacement of MockBean
	private TimeService timeService

	@Autowired
	private RandomService randomService

	def "time must be constant for our test"() {
		given:
			timeService.getTime() >> 1000

		expect:
			randomService.getNextRandom() == 1000 + 42
	}
}