package slides2_spock

import spock.lang.Specification

class AdditionTest2b extends Specification {
	def "add two numbers"() {
		when:
			def actual = Adder.add(1, 2)

		then:
			actual == 3
	}
}
