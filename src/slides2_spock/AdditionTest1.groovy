package slides2_spock

import junit.framework.Assert
import spock.lang.Specification

class AdditionTest1 extends Specification {
	def addTwoNumbers() {
		Assert.assertEquals(slides2_spock.Adder.add(1, 2), 3);
	}
}
