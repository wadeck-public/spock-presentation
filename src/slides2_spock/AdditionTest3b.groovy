package slides2_spock

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class AdditionTest3b extends Specification {
	def "add numbers: #a + #b = #expected"() {
		expect:
			Adder.add(a, b) == expected

		where:
			a << [1, 1, 1, 6]
			b << [1, -1, 4, 1]
			expected << [2, 0, 5, 7]
	}
}