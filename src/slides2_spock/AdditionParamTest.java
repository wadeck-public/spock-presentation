package slides2_spock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AdditionParamTest {
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
			{1, 1, 2}, {1, -1, 0}, {1, 4, 5}, {6, 1, 7}
		});
	}

	private int a;
	private int b;
	private int expected;

	public AdditionParamTest(int a, int b, int expected) {
		this.a = a;
		this.b = b;
		this.expected = expected;
	}

	@Test
	public void test() {
		assertEquals(expected, Adder.add(a, b));
	}
}